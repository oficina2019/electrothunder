﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroThunder.Database
{
    public class ProdutoDatabase
    {
        MySqlConnection con = new MySqlConnection("server=mysql7001.site4now.net;database=db_a4adbc_caua123;uid=a4adbc_caua123;pwd=NEWhMeTqp8a8ZkA");


        public void Inserir(Modelos.ProdutoModel p)
        {
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"insert into tb_produto(nm_produto,desc_produto, qtd_produto,vl_produto)
                                        values(@nm_produto, @desc_produto,@qtd_produto, @vl_produto)";


            comand.Parameters.Add(new MySqlParameter("nm_produto", p.Nome));
            comand.Parameters.Add(new MySqlParameter("desc_produto", p.Desc));
            comand.Parameters.Add(new MySqlParameter("qtd_produto", p.Qtd));
            comand.Parameters.Add(new MySqlParameter("vl_produto", p.Preco));

            comand.ExecuteNonQuery();
            con.Close();
        }
        public List<Modelos.ProdutoModel> Consultar()
        {
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = "select * from tb_produto";

            MySqlDataReader reader = comand.ExecuteReader();

            List<Modelos.ProdutoModel> lista = new List<Modelos.ProdutoModel>();
            while (reader.Read())
            {
                Modelos.ProdutoModel p = new Modelos.ProdutoModel()
                {
                    Id = Convert.ToInt32(reader["id_produto"]),
                    Nome = Convert.ToString(reader["nm_produto"]),
                    Desc = Convert.ToString(reader["desc_produto"]),
                    Qtd = Convert.ToInt32(reader["qtd_produto"]),
                    Preco = Convert.ToDecimal(reader["vl_produto"])
                };
                lista.Add(p);
            }
            con.Close();

            return lista;
        }


        public Modelos.ProdutoModel BuscarPorIdPro(int ID)
        {
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"select * from tb_produto where id_produto = @id_produto";

            comand.Parameters.Add(new MySqlParameter("id_produto", ID));

            MySqlDataReader reader = comand.ExecuteReader();

            Modelos.ProdutoModel model = null;

            if (reader.Read())
            {
                model = new Modelos.ProdutoModel
                {
                    Id = Convert.ToInt32(reader["id_produto"]),
                    Nome = Convert.ToString(reader["nm_produto"]),
                    Desc = Convert.ToString(reader["desc_produto"]),
                    Qtd = Convert.ToInt32(reader["qtd_produto"]),
                    Preco = Convert.ToDecimal(reader["vl_produto"])
                };
            }
            con.Close();


            return model;
        }

        public void DelePorIdPro(int ID)
        {
            con.Open();

            MySqlCommand command = con.CreateCommand();
            command.CommandText = "delete from tb_produto where id_produto = @id_produto";

            command.Parameters.Add(new MySqlParameter("id_produto", ID));

            command.ExecuteNonQuery();
            con.Close();
        }
        public void Alterar(Modelos.ProdutoModel p)
        {
            con.Open();

            MySqlCommand comand = con.CreateCommand();
            comand.CommandText = @"update tb_produto
                               set nm_produto = @nm_produto,
                               desc_produto = @desc_produto,
                               qtd_produto = @qtd_produto,
                               vl_produto = @vl_produto
                               where id_filme = @id_filme";

            comand.Parameters.Add(new MySqlParameter("nm_produto", p.Nome));
            comand.Parameters.Add(new MySqlParameter("desc_produto", p.Desc));
            comand.Parameters.Add(new MySqlParameter("qtd_produto", p.Qtd));
            comand.Parameters.Add(new MySqlParameter("vl_produto", p.Preco));

            comand.ExecuteNonQuery();
            con.Close();

        }
    }
}