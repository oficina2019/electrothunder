﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroThunder.Business
{
    public class ProdutoBusiness
    {
        public void Inserir(Modelos.ProdutoModel p)
        {
            Database.ProdutoDatabase produto = new Database.ProdutoDatabase();
            produto.Inserir(p);
        }
        public List<Modelos.ProdutoModel> Consultar()
        {
            Database.ProdutoDatabase produto = new Database.ProdutoDatabase();
            List<Modelos.ProdutoModel> lista = produto.Consultar();

            return lista;
        }

        public Modelos.ProdutoModel Buscarid(int ID)
        {
            Database.ProdutoDatabase produto = new Database.ProdutoDatabase();
            Modelos.ProdutoModel produtoModel = produto.BuscarPorIdPro(ID);

            return produtoModel;
        }

        public void Deletarpro(int ID)
        {
            Database.ProdutoDatabase produto = new Database.ProdutoDatabase();
            produto.DelePorIdPro(ID);
        }
        public void Akterar(Modelos.ProdutoModel p)
        {
            Database.ProdutoDatabase produto = new Database.ProdutoDatabase();
            produto.Alterar(p);
        }
    }
}