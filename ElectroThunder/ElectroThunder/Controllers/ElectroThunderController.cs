﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElectroThunder.Controllers
{
    public class ElectroThunderController : Controller
    {
        public ActionResult AlterarProdutos()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CadastrarProduto(Modelos.ProdutoModel p)
        {
            Business.ProdutoBusiness business = new Business.ProdutoBusiness();
            business.Inserir(p);
            return View();
        }
        public ActionResult CadastrarProduto()
        {
            return View();
        }
        public ActionResult ConsultarProduto()
        {
            Business.ProdutoBusiness business = new Business.ProdutoBusiness();
            return View(business.Consultar());
        }
        public ActionResult Contatos()
        {
            return View();
        }
        public ActionResult QuemSomos()
        {
            return View();
        }
        public ActionResult Inicio()
        {
            return View();
        }
        public ActionResult DeletarProduto(int ID,Modelos.ProdutoModel model)
        {
            Business.ProdutoBusiness business = new Business.ProdutoBusiness();
            Modelos.ProdutoModel produtoModel1 = business.Buscarid(ID);

            return View(model);
        }
        [HttpPost]
        public ActionResult DeletarPruduto(int id)
        {
            Business.ProdutoBusiness business = new Business.ProdutoBusiness();
            business.Deletarpro(id);

            return RedirectToAction("Consultar");
        }
        public ActionResult Alterar()
        {
            return View();
        }
        public ActionResult Alterar(int id)
        {
            Business.ProdutoBusiness business = new Business.ProdutoBusiness();
            Modelos.ProdutoModel p = new Modelos.ProdutoModel();

            return View(p);
        }

    }
}