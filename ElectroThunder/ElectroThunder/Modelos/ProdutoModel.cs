﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectroThunder.Modelos
{
    public class ProdutoModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Desc { get; set; }

        public int Qtd { get; set; }

        public decimal Preco { get; set; }
    }
}